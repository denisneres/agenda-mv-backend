# Agenda-MV (BACKEND)

Projeto de uma agenda de contatos, com dados pessoais e telefones.

## Rodando a Aplicação

com maven instalado na máquina, rode: `mvn package && java -jar target/agenda-0.0.1-SNAPSHOT.jar`, ou suba na sua IDE de preferência

## Persistência dos dados

para via de teste, o sistema está salvando em um banco em memória, H2, que pode ser acesso em  `localhost:8080/h2-console`

OBS: Por ser em memória, os dados sempre serão apagados quando o sistema reiniciar.

OBS2: - JPA usado para persistência usando **Spring Data** (usando repositories como interface, e *query methods*)



