package br.com.demo.mv.agenda.controllers;

import br.com.demo.mv.agenda.entities.Pessoa;
import br.com.demo.mv.agenda.services.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/pessoa")
public class PessoaRestController {

	@Autowired
	PessoaService pessoaService;

	@PostMapping
	public ResponseEntity<Pessoa> create(@RequestBody Pessoa pessoa) {

		try {

			Pessoa pessoaCriada = this.pessoaService.createOrUpdate(pessoa);

			return ResponseEntity.ok(pessoaCriada);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@PutMapping
	public ResponseEntity<Pessoa> update(@RequestBody Pessoa pessoa) {

		try {
			Pessoa pessoaAtualizada = this.pessoaService.createOrUpdate(pessoa);
			return ResponseEntity.ok(pessoaAtualizada);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@GetMapping ResponseEntity<List<Pessoa>> findAll() {
		try {
			List<Pessoa> pessoas = this.pessoaService.findAll();
			return ResponseEntity.ok(pessoas);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@GetMapping(value = "/{id}")
	private ResponseEntity<Pessoa> findById(@PathVariable("id") String id) {

		try {
			Pessoa pessoa = this.pessoaService.findById(Long.valueOf(id)).orElseThrow(() -> new Exception());
			return ResponseEntity.ok(pessoa);

		} catch (Exception e) {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@DeleteMapping(value = "/{id}")
	private ResponseEntity<String> delete(@PathVariable("id") String id) {

		try {
			this.pessoaService.delete(Long.valueOf(id));
			return ResponseEntity.ok("Removido com sucesso");
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("error");
		}
	}
}
