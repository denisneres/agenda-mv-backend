package br.com.demo.mv.agenda.repositories;

import br.com.demo.mv.agenda.entities.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {}
