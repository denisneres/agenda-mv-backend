package br.com.demo.mv.agenda.services;

import br.com.demo.mv.agenda.entities.Pessoa;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Component
public interface PessoaService {

	Pessoa createOrUpdate(Pessoa pessoa);

	Optional<Pessoa> findById(Long id);

	void delete (Long id);

	List<Pessoa> findAll();
}
