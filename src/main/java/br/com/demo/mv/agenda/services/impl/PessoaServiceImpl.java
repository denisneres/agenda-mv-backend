package br.com.demo.mv.agenda.services.impl;

import br.com.demo.mv.agenda.entities.Pessoa;
import br.com.demo.mv.agenda.repositories.PessoaRepository;
import br.com.demo.mv.agenda.services.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PessoaServiceImpl implements PessoaService {

	@Autowired
	PessoaRepository pessoaRepository;

	@Override
	public Pessoa createOrUpdate(Pessoa pessoa) {
		return this.pessoaRepository.save(pessoa);
	}

	@Override
	public Optional<Pessoa> findById(Long id) {
		return this.pessoaRepository.findById(id);
	}

	@Override
	public void delete(Long id) {
		Pessoa pessoa = this.pessoaRepository.findById(id).get();
		this.pessoaRepository.delete(pessoa);
	}

	@Override
	public List<Pessoa> findAll() {
		return this.pessoaRepository.findAll();
	}
}
